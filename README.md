1. clone the repository

2. create folder app/code/Mbs/PunchHole when located at the root of the Magento site

3. copy the content of this repository within the folder

4. install the module php bin/magento setup:upgrade
5. add something to the cart then from a product page, verify the API block appears
6. verify that each time the add to cart button is hit, the rendering changes regardless of the full page cache status



